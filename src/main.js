import '@/plugins/vue-composition-api'
// import '@/styles/styles.scss'
import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import VueResource from "vue-resource";
import { TimeFormate12h, WhatsappuserPhone } from "./filters";
import config from 'dotenv';

config.config();
Vue.config.productionTip = false;

Vue.use(VueResource);

Vue.filter("TimeFormat12H", TimeFormate12h);
Vue.filter("WppUserPhone", WhatsappuserPhone);

Vue.http.interceptors.push((request, next) => {
  request.headers.set('Authorization', 'Bearer ' + localStorage.tlaKey)
  request.headers.set('Accept', 'application/json')
  request.headers.set('Access-Control-Allow-Origin', '*')
  request.headers.set('Access-Control-Allow-Methods','GET,POST,PUT,PATCH,DELETE');
  request.headers.set('Access-Control-Allow-Methods','Content-Type','Authorization');
  next()
});

if(process.env.NODE_ENV == 'development'){
  Vue.http.options.root = 'http://localhost:3004/';
}else{
  Vue.http.options.root = process.env.VUE_APP_RUTA_API;
}

new Vue({
  router,
  store,
  vuetify,
  render: function(h) {
    return h(App);
  },
}).$mount("#app");


